# krishisathibackend


# Description
KrishiSathi is an Ecommerce website where a seller can register himself and sell various products of multiple categories which is used in farming. And a buyer can register himself and buy the products . It also recommend the farmer to grow best crop in any region of India in any season.

Tools- Vscode , Eclipse ,Mysql ,Apache Tomcat (BackEnd)

Technology- Spring Boot ,Angular, Mysql

KrishiSathiBackEnd is the back end image of Krishi Sathi Ecommerce Website on base image openjdk:18 and inbuild server is  apche tomcat.


## Features

-  User Registration and Authentication using Gmail OTP and Google Verification

-  User login using Password and Google Verification

- Seller/Customer Dashboard

- Product Listings

- Shopping Cart and Checkout

- Order Management

- Crop Recommendations for farmer to  grow on basis of location and season.


## Usage

- Seller (Admin)

  1- Create ,Update ,Delete  Categories
   
  2- Create ,Update ,Delete Product


- Customer (User)

  1- Add Product to cart

  2- Remove product from cart

  3- Checkout products



## Requirements

- Linux OS/Environment
- Docker
- Git

## Run Locally

1- Create a network in docker named as krishisathi-network
```bash
docker network create krishisathi-network
``` 

2- Pull a mysql db and run on krishisathi-network and create a database named as krishisathi
```bash
docker pull mysql:latest
docker run --name mysqlContainer1 --network krishisathi-network -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=krishisathi -d mysql
``` 

3- Pull  saurabhkumarr99/krishisathibackend:v1.1 and run on the krishisathi-network
```bash  
docker pull saurabhkumarr99/krishisathibackend:v1.1
docker run --name krishisathibackendContainer1 --network krishisathi-network -it -p 8085:8085  saurabhkumarr99/krishisathibackend:v1.1
``` 
Click on http://localhost:8085/getAllUsers

Click on http://localhost:8085/verifyEmail/xyz@gmail.com

4- Pull saurabhkumarr99/krishisathifrontend:v1.1 and run the image
```bash
docker pull saurabhkumarr99/krishisathifrontend:v1.1
docker run --name krishisathifrontendContainer1  -p 4200:80 saurabhkumarr99/krishisathifrontend:v1.1
``` 

Click on http://localhost4200

### Using Script File
1- Download script file [krishisathi](https://gitlab.com/saurabhkumarr99/krishisathifrontend/-/raw/master/startKrishiSathi.sh?inline=false)

2- Open terminal where script file available and then execute
```bash  
chmod +x krishisathi.sh
./krishisathi.sh
``` 
Click on http://localhost:4200

## Authors

- SAURABH KUMAR RAI



## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://saurabhkumarr99.github.io/MyResume/)
[![twitter](https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/@saurabhkumarr99)
